# Back-party

## Pour démarrer le projet, veuillez suivre les étapes ci-dessous :

### 1 - Configurer l'application :
Ouvrez le fichier application.properties et remplacez les champs avec les valeurs appropriées pour votre environnement.

### 2 - Installation des dépendances
Exécutez la commande suivante : mvn clean install

### Lancer le serveur :
Démarrez le serveur avec la commande ci-dessous. Le serveur sera accessible à l'adresse suivante :
http://localhost:8081/

## Utilisation des Endpoints

Enregistrer un nouvel utilisateur
Endpoint : POST http://localhost:8081/api/v1/user/save
Description : Cet endpoint permet d'enregistrer un nouvel utilisateur dans le système.

Connexion d'un utilisateur
Endpoint : POST http://localhost:8081/api/v1/user/login
Description : Cet endpoint permet à un utilisateur de se connecter.
