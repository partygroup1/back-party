#!/bin/bash

# Define the API endpoint
API_ENDPOINT="http://localhost:8081/api/board-game-events"

# Create a JSON payload for the BoardGameEvent
JSON_PAYLOAD=$(cat <<EOF
{
  "name": "Family Game Night",
  "location": "Community Center",
  "type": "Board Game",
  "dateTime": "2024-07-15T18:00:00",
  "totalSlots": 20,
  "remainingSlots": 20,
  "isPaid": false,
  "price": 0.0,
  "description": "An evening of fun board games for families.",
  "createdAt": "2024-06-28T12:00:00",
  "updatedAt": "2024-06-28T12:00:00",
  "gameTitle": "Monopoly",
  "gameDescription": "A classic property trading game.",
  "gameMinPlayers": 2,
  "gameMaxPlayers": 6
}
EOF
)

# Use curl to send a POST request to the API endpoint with the JSON payload
curl -X POST $API_ENDPOINT \
  -H "Content-Type: application/json" \
  -d "$JSON_PAYLOAD"

# Output the result
echo "BoardGameEvent added successfully"
