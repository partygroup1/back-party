package com.example.projetparty.repository;

import com.example.projetparty.entities.ClassicEvent;
import com.example.projetparty.entities.Event;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ClassicEventRepository extends EventRepository<ClassicEvent> {
    Optional<Event> findByName(String name);

    // Find all events by location
    List<Event> findByLocation(String location);

    // Find all events by organizer
    List<Event> findByOrganizerId(Long organizerId);

    // Custom query to find events by type
    List<Event> findByType(String type);

    // Custom query to find upcoming events
    List<Event> findByDateTimeAfter(LocalDateTime dateTime);
}
