package com.example.projetparty.repository;

import com.example.projetparty.entities.BoardGameEvent;
import com.example.projetparty.entities.Event;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BoardGameEventRepository extends EventRepository<BoardGameEvent> {

    @Query("SELECT b.gameList FROM BoardGameEvent b WHERE b.id = :eventId")
    List<String> findGameListByEventId(Long eventId);

    boolean existsByBringYourOwnGames(boolean bringYourOwnGames);
    Optional<Event> findByName(String name);

    // Find all events by location
    List<Event> findByLocation(String location);

    // Find all events by organizer
    List<Event> findByOrganizerId(Long organizerId);

    // Custom query to find events by type
    List<Event> findByType(String type);

    // Custom query to find upcoming events
    List<Event> findByDateTimeAfter(LocalDateTime dateTime);


}
