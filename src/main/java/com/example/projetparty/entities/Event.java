package com.example.projetparty.entities;


import com.example.projetparty.authentification.entity.User;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.BatchSize;

import java.time.LocalDateTime;
import java.util.List;
@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "events", indexes = {
        @Index(name = "idx_event_id", columnList = "id"),
        @Index(name = "idx_event_name", columnList = "name"),
        @Index(name = "idx_event_price", columnList = "price"),
        @Index(name = "idx_event_type", columnList = "type")
})
@BatchSize(size = 10)
public abstract class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String location;
    private String type;
    private LocalDateTime dateTime;
    private int totalSlots;
    private int remainingSlots;
    private boolean isPaid;
    private double price;
    private String description;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    @OneToMany(mappedBy = "event")
    @BatchSize(size = 10)
    private List<Participant> participants ;
    @ManyToOne
    @JoinColumn(name = "organizer_id")
    private User organizer;

    protected Event() {
        // protected no-argument constructor
    }

}

