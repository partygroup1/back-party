package com.example.projetparty.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Table(name = "board_game_events")
public class BoardGameEvent extends Event {
    @ElementCollection
    @CollectionTable(name = "game_list", joinColumns = @JoinColumn(name = "event_id"))
    @Column(name = "game")
    private List<String> gameList;
    private boolean bringYourOwnGames;

}

