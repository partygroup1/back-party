package com.example.projetparty.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;

import java.util.List;

@Entity
@Table(name = "video_game_events")
@AllArgsConstructor
@Data
@NoArgsConstructor
@BatchSize(size = 10)
public class VideoGameEvent extends Event {
    @ElementCollection
    @CollectionTable(name = "video_game_list", joinColumns = @JoinColumn(name = "event_id"))
    @Column(name = "game")
    @BatchSize(size = 10)
    private List<String> videoGameList;

    private boolean bringYourOwnDevices;

}
