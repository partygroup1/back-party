package com.example.projetparty.controller;

import com.example.projetparty.dto.BoardGameEventDto;
import com.example.projetparty.dto.ClassicEventDto;
import com.example.projetparty.dto.EventDto;
import com.example.projetparty.dto.VideoGameEventDto;
import com.example.projetparty.service.BoardGameEventService;
import com.example.projetparty.service.ClassicEventService;
import com.example.projetparty.service.VideoGameEventService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class HomeController {
    @Autowired
    private BoardGameEventService boardGameEventService;
    @Autowired
    private VideoGameEventService videoGameEventService;
    @Autowired
    private ClassicEventService classicEventService;

    @Cacheable(value = "allEventsCache")
    @GetMapping("/events")
    public List<EventDto> getAllEvents() {
        List<EventDto> all = new ArrayList<>();
        List<BoardGameEventDto> boardGames =  boardGameEventService.findAll();
        List<ClassicEventDto> classics = classicEventService.findAll();
        List<VideoGameEventDto> videoGames = videoGameEventService.findAll();
        all.addAll(classics);
        all.addAll(boardGames);
        all.addAll(videoGames);
        return all;
    }

    @GetMapping("/events/{id}")
    public ResponseEntity<EventDto> getEventById(@PathVariable Long id) {
        Optional<BoardGameEventDto> boardGameEvent = boardGameEventService.findById(id);
        if (boardGameEvent.isPresent()) {
            return ResponseEntity.ok(boardGameEvent.get());
        }

        Optional<ClassicEventDto> classicEvent = classicEventService.findById(id);
        if (classicEvent.isPresent()) {
            return ResponseEntity.ok(classicEvent.get());
        }

        Optional<VideoGameEventDto> videoGameEvent = videoGameEventService.findById(id);
        if (videoGameEvent.isPresent()) {
            return ResponseEntity.ok(videoGameEvent.get());
        }

        return ResponseEntity.notFound().build();
    }


///////////////////search methods
    @GetMapping("/events/location/{location}")
    public List<EventDto> getEventsByLocation(@PathVariable String location) {
        List<BoardGameEventDto> boardGames = boardGameEventService.findByLocation(location);
        List<ClassicEventDto> classics = classicEventService.findByLocation(location);
        List<VideoGameEventDto> videoGames = videoGameEventService.findByLocation(location);
        List<EventDto> all = new ArrayList<>();
        all.addAll(boardGames);
        all.addAll(classics);
        all.addAll(videoGames);
        return all;
    }

    @GetMapping("/events/organizer/{organizerId}")
    public List<EventDto> getEventsByOrganizerId(@PathVariable Long organizerId) {
        List<EventDto> all = boardGameEventService.findByOrganizerId(organizerId).stream()
                .map(EventDto.class::cast)
                .collect(Collectors.toList());
        all.addAll(classicEventService.findByOrganizerId(organizerId).stream()
                .map(EventDto.class::cast)
                .collect(Collectors.toList()));
        all.addAll(videoGameEventService.findByOrganizerId(organizerId).stream()
                .map(EventDto.class::cast)
                .collect(Collectors.toList()));
        return all;
    }

    @GetMapping("/events/type/{type}")
    public List<EventDto> getEventsByType(@PathVariable String type) {
        List<EventDto> all = new ArrayList<>();
        if(type.equals("bored-game")) {
            all = boardGameEventService.findByType(type).stream()
                    .map(EventDto.class::cast)
                    .collect(Collectors.toList());
        } else if (type.equals("classic-event")) {
            all.addAll(classicEventService.findByType(type).stream()
                    .map(EventDto.class::cast)
                    .collect(Collectors.toList()));
        } else if (type.equals("video-game-event")) {
            all.addAll(videoGameEventService.findByType(type).stream()
                    .map(EventDto.class::cast)
                    .collect(Collectors.toList()));
        }
        return all;
    }

    @Cacheable(value = "upcomingEventsCache")
    @GetMapping("/events/upcoming")
    public List<EventDto> getUpcomingEvents(@RequestParam LocalDateTime dateTime) {
        List<EventDto> all = boardGameEventService.findByDateTimeAfter(dateTime).stream()
                .map(EventDto.class::cast)
                .collect(Collectors.toList());
        all.addAll(classicEventService.findByDateTimeAfter(dateTime).stream()
                .map(EventDto.class::cast)
                .collect(Collectors.toList()));
        all.addAll(videoGameEventService.findByDateTimeAfter(dateTime).stream()
                .map(EventDto.class::cast)
                .collect(Collectors.toList()));
        return all;
    }

    @Cacheable(value = "boardGameEventsCache")
    @GetMapping("/board-game-events")
    public List<BoardGameEventDto> getAllBoardGameEvents() {
        return boardGameEventService.findAll().stream()
                .map(BoardGameEventDto::new)
                .collect(Collectors.toList());
    }

    @CacheEvict(value = {"allEventsCache", "boardGameEventsCache"}, allEntries = true)
    @PostMapping("/board-game-events")
    public ResponseEntity<BoardGameEventDto> saveBoardGameEvent(@RequestBody BoardGameEventDto eventDto) {
        BoardGameEventDto savedEvent = boardGameEventService.save(eventDto);
        return ResponseEntity.ok(savedEvent);
    }

    @CachePut(value = "boardGameEventCache", key = "#id")
    @CacheEvict(value = {"boardGameEventsCache", "allEventsCache"}, allEntries = true)
    @PutMapping("/board-game-events/{id}")
    public ResponseEntity<BoardGameEventDto> updateBoardGameEvent(@PathVariable long id, @RequestBody BoardGameEventDto eventDto) {
        return boardGameEventService.update(id, eventDto)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Cacheable(value = "videoGameEventsCache")
    @GetMapping("/video-game-events")
    public List<VideoGameEventDto> getAllVideoGameEvents() {
        return videoGameEventService.findAll().stream()
                .map(VideoGameEventDto.class::cast)
                .collect(Collectors.toList());
    }

    @Cacheable(value = "classicEventsCache")
    @GetMapping("/classic-events")
    public List<ClassicEventDto> getAllClassicEvents() {
        return classicEventService.findAll().stream()
                .map(ClassicEventDto.class::cast)
                .collect(Collectors.toList());
    }
}


