package com.example.projetparty.utils;

public enum Status {
    PENDING,
    CONFIRMED,
    CANCELLED
}
