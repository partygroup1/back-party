package com.example.projetparty.utils;

public enum PaymentStatus {
    NOT_PAID,
    PAID,
    REFUNDED
}
