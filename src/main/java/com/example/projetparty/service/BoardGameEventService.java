package com.example.projetparty.service;

import com.example.projetparty.dto.BoardGameEventDto;
import com.example.projetparty.entities.BoardGameEvent;
import com.example.projetparty.mapper.BoardGameEventMapper;
import com.example.projetparty.repository.BoardGameEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BoardGameEventService extends EventService<BoardGameEvent, BoardGameEventDto> {

    private final BoardGameEventMapper boardGameEventMapper;
    private final BoardGameEventRepository boardGameEventRepository;

    @Autowired
    public BoardGameEventService(BoardGameEventRepository boardGameEventRepository, BoardGameEventMapper boardGameEventMapper) {
        super(boardGameEventRepository, boardGameEventMapper);
        this.boardGameEventRepository = boardGameEventRepository;
        this.boardGameEventMapper = boardGameEventMapper;
    }

    @Override
    public BoardGameEventDto save(BoardGameEventDto eventDto) {
        BoardGameEvent event = boardGameEventMapper.fromDto(eventDto);
        BoardGameEvent savedEvent = boardGameEventRepository.save(event);
        return boardGameEventMapper.toDto(savedEvent);
    }

    @Override
    public Optional<BoardGameEventDto> update(long id, BoardGameEventDto eventDto) {
        Optional<BoardGameEvent> optionalEvent = boardGameEventRepository.findById(id);
        if (optionalEvent.isPresent()) {
            BoardGameEvent boardGameEvent = optionalEvent.get();
            boardGameEventMapper.updateFromDto(eventDto, boardGameEvent); // Update entity fields from DTO
            boardGameEvent.setId(id);
            BoardGameEvent updatedBoardGameEvent = boardGameEventRepository.save(boardGameEvent);
            return Optional.of(boardGameEventMapper.toDto(updatedBoardGameEvent));
        }
        return Optional.empty();
    }

}