package com.example.projetparty.service;

import com.example.projetparty.dto.ClassicEventDto;
import com.example.projetparty.entities.ClassicEvent;
import com.example.projetparty.mapper.ClassicEventMapper;
import com.example.projetparty.repository.ClassicEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClassicEventService extends EventService<ClassicEvent, ClassicEventDto> {

    private final ClassicEventMapper classicEventMapper;

    @Autowired
    public ClassicEventService(ClassicEventRepository classicEventRepository, ClassicEventMapper classicEventMapper) {
        super(classicEventRepository, classicEventMapper);
        this.classicEventMapper = classicEventMapper;
    }

    @Override
    public ClassicEventDto save(ClassicEventDto eventDto) {
        ClassicEvent classicEvent = classicEventMapper.fromDto(eventDto);
        return classicEventMapper.toDto(eventRepository.save(classicEvent));
    }

    @Override
    public Optional<ClassicEventDto> update(long id, ClassicEventDto eventDto) {
        Optional<ClassicEvent> optionalEvent = eventRepository.findById(id);
        if (optionalEvent.isPresent()) {
            ClassicEvent classicEvent = optionalEvent.get();
            classicEventMapper.updateFromDto(eventDto, classicEvent); // Update entity fields from DTO
            classicEvent.setId(id);
            classicEvent.getParticipants().forEach(participant -> participant.setEvent(classicEvent));
            ClassicEvent updatedClassicEvent = eventRepository.save(classicEvent);
            return Optional.of(classicEventMapper.toDto(updatedClassicEvent));
        }
        return Optional.empty();
    }
}