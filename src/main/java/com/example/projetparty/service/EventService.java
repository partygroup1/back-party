package com.example.projetparty.service;

import com.example.projetparty.dto.EventDto;
import com.example.projetparty.entities.Event;
import com.example.projetparty.mapper.EventMapper;
import com.example.projetparty.repository.EventRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public abstract class EventService<T extends Event, D extends EventDto> {
    protected final EventRepository<T> eventRepository;
    protected final EventMapper<T, D> eventMapper;

    public List<D> findAll() {
        List<T> events = eventRepository.findAll();
        return events.stream()
                .map(eventMapper::toDto)
                .collect(Collectors.toList());
    }

    public Optional<D> findById(long id) {
        return eventRepository.findById(id)
                .map(eventMapper::toDto);
    }

    public D save(D eventDto) {
        T event = eventMapper.fromDto(eventDto);
        event.getParticipants().forEach(participant -> participant.setEvent(event));
        return eventMapper.toDto(eventRepository.save(event));
    }

    public void delete(long id) {
        eventRepository.deleteById(id);
    }

    public Optional<D> update(long id, D eventDto) {
        if (eventRepository.existsById(id)) {
            T event = eventMapper.fromDto(eventDto);
            event.setId(id);
            event.getParticipants().forEach(participant -> participant.setEvent(event));
            return Optional.of(eventMapper.toDto(eventRepository.save(event)));
        }
        return Optional.empty();
    }



    public List<D> findByLocation(String location) {
        List<T> events = (List<T>) eventRepository.findByLocation(location);
        return events.stream()
                .map(eventMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<D> findByOrganizerId(Long organizerId) {
        List<T> events = (List<T>) eventRepository.findByOrganizerId(organizerId);
        return events.stream()
                .map(eventMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<D> findByType(String type) {
        List<T> events = (List<T>) eventRepository.findByType(type);
        return events.stream()
                .map(eventMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<D> findByDateTimeAfter(LocalDateTime dateTime) {
        List<T> events = (List<T>) eventRepository.findByDateTimeAfter(dateTime);
        return events.stream()
                .map(eventMapper::toDto)
                .collect(Collectors.toList());
    }

}

