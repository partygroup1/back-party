package com.example.projetparty.service;

import com.example.projetparty.dto.VideoGameEventDto;
import com.example.projetparty.entities.VideoGameEvent;
import com.example.projetparty.mapper.VideoGameEventMapper;
import com.example.projetparty.repository.VideoGameEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VideoGameEventService extends EventService<VideoGameEvent, VideoGameEventDto> {

    private final VideoGameEventMapper videoGameEventMapper;
    private final VideoGameEventRepository videoGameEventRepository;

    @Autowired
    public VideoGameEventService(VideoGameEventRepository videoGameEventRepository, VideoGameEventMapper videoGameEventMapper) {
        super(videoGameEventRepository, videoGameEventMapper);
        this.videoGameEventRepository = videoGameEventRepository;
        this.videoGameEventMapper = videoGameEventMapper;
    }

    @Override
    public VideoGameEventDto save(VideoGameEventDto eventDto) {
        VideoGameEvent videoGameEvent = videoGameEventMapper.fromDto(eventDto);
        videoGameEvent.getParticipants().forEach(participant -> participant.setEvent(videoGameEvent));
        VideoGameEvent savedVideoGameEvent = videoGameEventRepository.save(videoGameEvent);
        return videoGameEventMapper.toDto(savedVideoGameEvent);
    }

    @Override
    public Optional<VideoGameEventDto> update(long id, VideoGameEventDto eventDto) {
        Optional<VideoGameEvent> optionalEvent = videoGameEventRepository.findById(id);
        if (optionalEvent.isPresent()) {
            VideoGameEvent videoGameEvent = optionalEvent.get();
            videoGameEventMapper.updateFromDto(eventDto, videoGameEvent); // Update entity fields from DTO
            videoGameEvent.setId(id);
            VideoGameEvent updatedVideoGameEvent = videoGameEventRepository.save(videoGameEvent);
            return Optional.of(videoGameEventMapper.toDto(updatedVideoGameEvent));
        }
        return Optional.empty();
    }
}