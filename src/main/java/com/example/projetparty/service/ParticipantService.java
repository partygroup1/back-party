package com.example.projetparty.service;

import com.example.projetparty.dto.ParticipantDto;
import com.example.projetparty.entities.Participant;
import com.example.projetparty.mapper.ParticipantMapper;
import com.example.projetparty.repository.ParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ParticipantService {

    private final ParticipantRepository participantRepository;
    @Autowired
    private  ParticipantMapper participantMapper;

    public List<ParticipantDto> findAll() {
        List<Participant> participants = participantRepository.findAll();
        return participants.stream()
                .map(participantMapper::toDto)
                .collect(Collectors.toList());
    }

    public Optional<ParticipantDto> findById(long id) {
        return participantRepository.findById(id)
                .map(participantMapper::toDto);
    }

    public ParticipantDto save(ParticipantDto participantDto) {
        Participant participant = participantMapper.fromDto(participantDto);
        return participantMapper.toDto(participantRepository.save(participant));
    }

    public void delete(long id) {
        participantRepository.deleteById(id);
    }

    public Optional<ParticipantDto> update(long id, ParticipantDto participantDto) {
        if (participantRepository.existsById(id)) {
            Participant participant = participantMapper.fromDto(participantDto);
            participant.setId(id);
            return Optional.of(participantMapper.toDto(participantRepository.save(participant)));
        }
        return Optional.empty();
    }
}
