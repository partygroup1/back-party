package com.example.projetparty.mapper;

import com.example.projetparty.dto.BoardGameEventDto;
import com.example.projetparty.entities.BoardGameEvent;
import org.springframework.stereotype.Component;

@Component
public class BoardGameEventMapper extends EventMapper<BoardGameEvent, BoardGameEventDto> {
    @Override
    public BoardGameEventDto toDto(BoardGameEvent boardGameEvent) {
        return modelMapper.map(boardGameEvent, BoardGameEventDto.class);
    }

    @Override
    public BoardGameEvent fromDto(BoardGameEventDto boardGameEventDto) {
        return modelMapper.map(boardGameEventDto, BoardGameEvent.class);
    }
    public void updateFromDto(BoardGameEventDto boardGameEventDto, BoardGameEvent boardGameEvent) {
        modelMapper.map(boardGameEventDto, boardGameEvent);
    }
}