package com.example.projetparty.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class EventMapper<E, D> {
    @Autowired
    protected ModelMapper modelMapper;

    public abstract D toDto(E entity);

    public abstract E fromDto(D dto);
}