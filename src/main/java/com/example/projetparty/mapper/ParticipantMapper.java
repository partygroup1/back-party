package com.example.projetparty.mapper;

import com.example.projetparty.dto.ParticipantDto;
import com.example.projetparty.entities.Participant;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ParticipantMapper {
    @Autowired
    private ModelMapper modelMapper;

    public ParticipantDto toDto(Participant participant)
    {
        return modelMapper.map(participant, ParticipantDto.class);
    }

    public Participant fromDto(ParticipantDto participantDto) {
        return modelMapper.map(participantDto, Participant.class);
    }
}
