package com.example.projetparty.mapper;

import com.example.projetparty.dto.VideoGameEventDto;
import com.example.projetparty.entities.VideoGameEvent;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VideoGameEventMapper extends EventMapper<VideoGameEvent, VideoGameEventDto>{

    private final ModelMapper modelMapper;

    @Autowired
    public VideoGameEventMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public VideoGameEventDto toDto(VideoGameEvent videoGameEvent) {
        return modelMapper.map(videoGameEvent, VideoGameEventDto.class);
    }

    public VideoGameEvent fromDto(VideoGameEventDto videoGameEventDto) {
        return modelMapper.map(videoGameEventDto, VideoGameEvent.class);
    }

    public void updateFromDto(VideoGameEventDto videoGameEventDto, VideoGameEvent videoGameEvent) {
        modelMapper.map(videoGameEventDto, videoGameEvent);
    }
}
