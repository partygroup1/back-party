package com.example.projetparty.mapper;

import com.example.projetparty.dto.ClassicEventDto;
import com.example.projetparty.entities.ClassicEvent;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClassicEventMapper extends EventMapper<ClassicEvent, ClassicEventDto> {

    private final ModelMapper modelMapper;

    @Autowired
    public ClassicEventMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public ClassicEventDto toDto(ClassicEvent classicEvent) {
        return modelMapper.map(classicEvent, ClassicEventDto.class);
    }

    public ClassicEvent fromDto(ClassicEventDto classicEventDto) {
        return modelMapper.map(classicEventDto, ClassicEvent.class);
    }

    public void updateFromDto(ClassicEventDto classicEventDto, ClassicEvent classicEvent) {
        modelMapper.map(classicEventDto, classicEvent);
    }
}