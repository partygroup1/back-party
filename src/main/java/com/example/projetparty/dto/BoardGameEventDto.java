package com.example.projetparty.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BoardGameEventDto extends EventDto {
    private List<String> gameList;
    private boolean bringYourOwnGames;

    public BoardGameEventDto(EventDto eventDto) {
    }
}
