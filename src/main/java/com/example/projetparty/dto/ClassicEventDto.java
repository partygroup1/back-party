package com.example.projetparty.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassicEventDto extends EventDto{
    private String foodAndDrinks;

}
