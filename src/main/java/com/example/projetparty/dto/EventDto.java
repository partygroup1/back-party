package com.example.projetparty.dto;

import com.example.projetparty.authentification.entity.User;
import com.example.projetparty.entities.Participant;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventDto {

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)

    private Long id;
    private String name;
    private String location;
    private String type;
    private LocalDateTime dateTime;
    private int totalSlots;
    private int remainingSlots;
    private boolean isPaid;
    private double price;
    private String description;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private List<Participant> participants ;
    private User organizer;
}
