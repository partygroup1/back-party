package com.example.projetparty.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class VideoGameEventDto extends EventDto{
    private List<String> videoGameList;
    private boolean bringYourOwnDevices;
}
