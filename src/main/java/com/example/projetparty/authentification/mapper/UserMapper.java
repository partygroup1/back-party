package com.example.projetparty.authentification.mapper;

import com.example.projetparty.authentification.dto.UserDto;
import com.example.projetparty.authentification.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = UserMapper.class)
public interface UserMapper {
    User toEntity(UserDto userDto);
    UserDto toDto(User user);
    List<User> toEntities(List<UserDto> userDtos);
    List<UserDto> toDtos(List<User> users);
}
