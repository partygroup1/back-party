package com.example.projetparty.authentification.utils;
public enum FieldOfInterest {
    GAMING, ALCOHOL, DANCING, OTHER
}
