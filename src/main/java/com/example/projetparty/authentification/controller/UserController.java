package com.example.projetparty.authentification.controller;

import com.example.projetparty.authentification.dto.LoginDto;
import com.example.projetparty.authentification.dto.UserDto;
import com.example.projetparty.authentification.service.UserService;
import com.example.projetparty.authentification.utils.LoginMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    @Autowired
    private UserService userService;
    @PostMapping(path = "/save")
    public String saveUser(@RequestBody UserDto userDto)
    {
        return userService.addUser(userDto);
    }
    @PostMapping(path = "/login")
    public ResponseEntity<LoginMessage> loginUser(@RequestBody LoginDto loginDto)
    {
        LoginMessage loginResponse = userService.loginUser(loginDto);
        return ResponseEntity.ok(loginResponse);
    }
}
