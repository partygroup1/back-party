package com.example.projetparty.authentification.repository;

import com.example.projetparty.authentification.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findOneByEmailAndPassword(String email, String password);
    User findByEmail(String email);
}
