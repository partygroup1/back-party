package com.example.projetparty.authentification.entity;

import com.example.projetparty.entities.Event;
import com.example.projetparty.entities.Participant;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Table(name = "personne")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;
    @Column(name="user_name", length = 100)
    private String userName;
    @Column(name="email", length = 100)
    private String email;
    @Column(name="password", length = 100)
    private String password;

    @OneToMany(mappedBy = "organizer")
    private List<Event> organizedEvents;

    @OneToMany(mappedBy = "user")
    private List<Participant> participations;

}
