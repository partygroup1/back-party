package com.example.projetparty.authentification.service;

import com.example.projetparty.authentification.dto.LoginDto;
import com.example.projetparty.authentification.entity.User;
import com.example.projetparty.authentification.mapper.UserMapper;
import com.example.projetparty.authentification.repository.UserRepository;
import com.example.projetparty.authentification.dto.UserDto;
import com.example.projetparty.authentification.utils.LoginMessage;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private UserMapper userMapper;
    public String addUser(UserDto userDto) {
        User user = new User(
                userDto.getId(),
                userDto.getUserName(),
                userDto.getEmail(),
                this.passwordEncoder.encode(userDto.getPassword()),
                null,
                null
        );
        userRepository.save(user);
        return user.getUserName();
    }

    public LoginMessage loginUser(LoginDto loginDto) {
        User user1 = userRepository.findByEmail(loginDto.getEmail());
        if (user1 != null) {
            String password = loginDto.getPassword();
            String encodedPassword = user1.getPassword();
            Boolean isPwdRight = passwordEncoder.matches(password, encodedPassword);
            if (isPwdRight) {
                var user = userRepository.findOneByEmailAndPassword(loginDto.getEmail(), encodedPassword);
                var response = userMapper.toDto(user);

                if (user != null) {
                    return new LoginMessage("Login Success", true,  response.getId(), response.getUserName(), response.getEmail());
                } else {
                    return new LoginMessage("Login Failed", false, null, null, null);
                }
            } else {
                return new LoginMessage("password Not Match", false, null, null, null);
            }
        } else {
            return new LoginMessage("Email not exits", false, null, null, null);
        }
    }
}
