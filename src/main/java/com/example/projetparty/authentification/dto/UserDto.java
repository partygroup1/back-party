package com.example.projetparty.authentification.dto;

import com.example.projetparty.entities.Event;
import com.example.projetparty.entities.Participant;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    private String userName;
    private String email;
    private String password;

    private List<Event> organizedEvents;
    private List<Participant> participations;
}
